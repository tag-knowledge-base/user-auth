FROM python:3.8-slim-buster
ARG PYPI_USERNAME
ARG PYPI_PASSWORD
ARG PYPI_REPOSITORY
ARG AUTH_DB_USER
ARG AUTH_DB_PASSWORD
ARG AUTH_DB_HOST
ENV PYPI_USERNAME=$PYPI_USERNAME
ENV PYPI_PASSWORD=$PYPI_PASSWORD
ENV PYPI_REPOSITORY=$PYPI_REPOSITORY

WORKDIR /app
COPY Pipfile* ./src ./
RUN set -e && ls && \
	apt-get update && \
	apt-get install -y --no-install-recommends build-essential gcc && \
	pip install pipenv && \
	pipenv install --deploy --system --ignore-pipfile
ENV CONFIG_DIR=./userauth
ENV AUTH_DB_USER=$AUTH_DB_USER
ENV AUTH_DB_PASSWORD=$AUTH_DB_PASSWORD
ENV AUTH_DB_HOST=$AUTH_DB_HOST

ENTRYPOINT ["uvicorn", "--port=8001", "--host=0.0.0.0", "userauth.main:APP"]
