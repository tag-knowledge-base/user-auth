""" tasks.py """


import os
import invoke


LOCAL_NEXUS_REPO = os.getenv(
    "LOCAL_NEXUS_REPO", "206.189.186.153:8081/repository/pypi-repo/simple"
)
DOCKER_REPO = os.getenv("DOCKER_REPOSITORY", "206.189.186.153:8083")
APP_VERSION = "1.0.0-rc.1"
APP_NAME = "userauthservice"


@invoke.task()
def publish(c):
    """ publish """
    user = os.getenv("PYPI_USERNAME")
    password = os.getenv("PYPI_PASSWORD")
    c.run(f"docker login -u {user} -p {password} {DOCKER_REPO}")
    c.run(f"docker push {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}")


@invoke.task
def build(c):
    """ build docker """
    c.run("rm -rf ./dist")
    c.run(
        "docker build . "
        f"--build-arg APP_NAME={APP_NAME} "
        f"--build-arg APP_VERSION={APP_VERSION} "
        "--build-arg PYPI_USERNAME=$PYPI_USERNAME "
        "--build-arg PYPI_PASSWORD=$PYPI_PASSWORD "
        f"--build-arg PYPI_REPOSITORY={LOCAL_NEXUS_REPO} "
        "--build-arg AUTH_DB_USER=$AUTH_DB_USER "
        "--build-arg AUTH_DB_PASSWORD=$AUTH_DB_PASSWORD "
        "--build-arg AUTH_DB_HOST=$AUTH_DB_HOST "
        "--build-arg AUTH_ALEMBIC_SCRIPT_LOCATION=userauth "
        f"-t {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}"
    )


@invoke.task
def run(c):
    """ run app """
    c.run("uvicorn --port 8001 --host localhost src.userauth.main:APP --reload")
