""" exceptions.py """


class AiServiceException(Exception):
    """ AiServiceException """

    def __init__(
        self, localization_key: str, message: str, status_code: int,
    ):
        """ __init__ """
        super().__init__()
        self.localization_key = localization_key
        self.message = message
        self.status_code = status_code


class InternalServerError(AiServiceException):
    """ InternalServerError """

    def __init__(self,):
        """ __init__ """
        super().__init__("userauthservice-servererror", "Internal Server Error", 500)


class UserHeaderNotFoundException(AiServiceException):
    """ UserHeaderNotFoundException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-userheadernotfoundexception",
            "User header not included",
            400,
        )


class AggregateNotFoundException(AiServiceException):
    """ AggregateNotFoundException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-aggregatenotfoundexception", "Aggregate not found.", 404
        )


class UserAlreadyExistsException(AiServiceException):
    """ UserAlreadyExistsException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-useralreadyexistsexception", "User already exists.", 409
        )


class SongAlreadyExistsExceptions(AiServiceException):
    """ SongAlreadyExistsExceptions """

    def __init__(self):
        """ __init__ """
        super().__init__(
            "userauthservice-songalreadyexistsexception", "Song already exists.", 409,
        )


class SongDoesNotExistException(AiServiceException):
    """ SongDoesNotExistException """

    def __init__(self):
        """ __init__ """
        super().__init__(
            "userauthservice-songdoesnotexistexception", "Song does not exist.", 404,
        )


class TagAlreadyExistsException(AiServiceException):
    """ TagAlreadyExistsException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-tagalreadyexistsexception", "Tag already exists.", 409
        )


class CannotClearTagThatDoesNotExistException(AiServiceException):
    """ CannotClearTagThatDoesNotExistException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-cannotcleartagthatdoesnotexistexception",
            "Cannot clear tag that does not exist.",
            404,
        )


class UserDoesNotExistException(AiServiceException):
    """ UserDoesNotExistException """

    def __init__(self,):
        """ __init__ """
        super().__init__(
            "userauthservice-userdoesnotexistexception", "User does not exist.", 404,
        )
