""" database.py """


from databases import Database
import sqlalchemy

import configuration.configparser


class Connection:
    """ Connection """

    def __init__(self, app_config: configuration.configparser.Config) -> None:
        """ __init__ """
        self.config = app_config
        username = app_config.get_value("AUTH_DB_USER")
        password = app_config.get_value("AUTH_DB_PASSWORD")
        host = app_config.get_value("AUTH_DB_HOST")
        port = app_config.get_value("AUTH_DB_PORT")
        db_name = app_config.get_value("AUTH_DB_NAME")
        self.connection = Database(
            f"postgresql://{username}:{password}@{host}:{port}/{db_name}"
        )


class DbContext:
    """ DbContext """

    def __init__(self, connection: Connection) -> None:
        """ __init__ """
        self.connection = connection
        self.metadata = sqlalchemy.MetaData()
        self.aggregates = sqlalchemy.Table(
            "aggregates",
            self.metadata,
            sqlalchemy.Column(
                "event_id",
                sqlalchemy.Integer,
                autoincrement=True,
                primary_key=True,
                nullable=False,
            ),
            sqlalchemy.Column("aggregate_id", sqlalchemy.Text, nullable=False),
            sqlalchemy.Column("event", sqlalchemy.JSON, nullable=False),
            sqlalchemy.UniqueConstraint(
                "aggregate_id", "event_id", name="unique_aggregate_id_in_event_ids"
            ),
        )
        self.songs = sqlalchemy.Table(
            "songs",
            self.metadata,
            sqlalchemy.Column(
                "aggregate_id", sqlalchemy.Text, primary_key=True, nullable=False,
            ),
            sqlalchemy.Column("song", sqlalchemy.Text, nullable=False,),
            sqlalchemy.Column("album", sqlalchemy.Text, nullable=False,),
            sqlalchemy.Column(
                "artist", sqlalchemy.ARRAY(sqlalchemy.Text), nullable=False,
            ),
            sqlalchemy.Column("track_position", sqlalchemy.Integer, nullable=False,),
            sqlalchemy.UniqueConstraint(
                "aggregate_id", name="unique_aggregate_id_in_songs"
            ),
        )
        self.song_likes = sqlalchemy.Table(
            "song_like",
            self.metadata,
            sqlalchemy.Column(
                "user_aggregate_id", sqlalchemy.Text, primary_key=True, nullable=False,
            ),
            sqlalchemy.Column("song_aggregate_id", sqlalchemy.Text, nullable=False,),
            sqlalchemy.Column("attitude_type", sqlalchemy.Text, nullable=False,),
            sqlalchemy.Column(
                "created_on", sqlalchemy.DateTime(timezone=True), nullable=False,
            ),
            sqlalchemy.UniqueConstraint(
                "user_aggregate_id",
                "song_aggregate_id",
                name="unique_user_and_song_for_like_constraint",
            ),
        )
