""" dependencies.py """


import pinject

from . import database, config, aggregate
from .api.platform.v1.user import (
    createusercommandhandler,
    userdetailsaggregate,
    saveuseroncreateusereventhandler,
    userdataqueryhandler,
    clearusercommandhandler,
    songcreatedeventhandler,
    songdeletedeventhandler,
    likesongeventhandler,
    likesongcommandhandler,
)
from .api.platform.v1.song import (
    createsongcommandhandler,
    createsongeventhandler,
    deletesongcommandhandler,
    deletesongeventhandler,
    songdetailsaggregate,
    getsongqueryhandler,
)


class AppBindingSpec(pinject.BindingSpec):  # type: ignore
    """ AppBindingSpec """

    def configure(self, bind) -> None:  # type: ignore
        """ configure """
        bind("app_config", to_instance=config.APP_CONFIG)
        bind("db_context", to_class=database.DbContext, in_scope=pinject.PROTOTYPE)
        bind(
            "user_aggregate",
            to_class=userdetailsaggregate.UserAggregate,
            in_scope=pinject.PROTOTYPE,
        )
        bind(
            "song_aggregate",
            to_class=songdetailsaggregate.SongAggregate,
            in_scope=pinject.PROTOTYPE,
        )
        bind(
            "user_aggregate_repository",
            to_class=userdetailsaggregate.UserAggregateRepository,
            in_scope=pinject.PROTOTYPE,
        )
        bind(
            "song_aggregate_repository",
            to_class=songdetailsaggregate.SongAggregateRepository,
            in_scope=pinject.PROTOTYPE,
        )


APP_GRAPH = pinject.new_object_graph(
    modules=[
        config,
        database,
        aggregate,
        createusercommandhandler,
        userdetailsaggregate,
        saveuseroncreateusereventhandler,
        userdataqueryhandler,
        clearusercommandhandler,
        createsongcommandhandler,
        createsongeventhandler,
        deletesongcommandhandler,
        deletesongeventhandler,
        songdetailsaggregate,
        getsongqueryhandler,
        songcreatedeventhandler,
        songdeletedeventhandler,
        likesongeventhandler,
        likesongcommandhandler,
    ],
    binding_specs=[AppBindingSpec()],
)
