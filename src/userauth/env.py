#  type: ignore
""" env.py """


#  pylint: skip-file


import importlib.util
import logging
from pathlib import Path

from alembic import context
import sqlalchemy
import sqlalchemy.exc
import sqlalchemy.pool

import configuration.configparser


config = context.config


database_spec = importlib.util.spec_from_file_location(
    "database", Path(__file__).parent / "database.py"
)
database_module = importlib.util.module_from_spec(database_spec)
database_spec.loader.exec_module(database_module)


APP_CONFIG = configuration.configparser.ConfigParser.load(
    cwd=str(Path(__file__).parent.absolute())
)
username = APP_CONFIG.get_value("AUTH_DB_USER")
password = APP_CONFIG.get_value("AUTH_DB_PASSWORD")
db_name = APP_CONFIG.get_value("AUTH_DB_NAME")
host = APP_CONFIG.get_value("AUTH_DB_HOST")
port = APP_CONFIG.get_value("AUTH_DB_PORT")

engine = sqlalchemy.create_engine(
    f"postgresql+pg8000://{username}:{password}@{host}:{port}/template1",
    isolation_level="AUTOCOMMIT",
)
with engine.connect() as conn:
    try:
        conn.execute(f"CREATE DATABASE {db_name}")
    except sqlalchemy.exc.ProgrammingError as err:
        if err.orig.args[0]["M"] == f'database "{db_name}" already exists':
            pass
        else:
            raise err


engine.dispose()

config.set_main_option(
    "sqlalchemy.url",
    f"postgresql+pg8000://{username}:{password}@{host}:{port}/{db_name}",
)

logging.getLogger("alembic")

target_metadata = database_module.DbContext(APP_CONFIG).metadata


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = sqlalchemy.engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=sqlalchemy.pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
