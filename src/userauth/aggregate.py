""" aggregate.py """


import abc
from collections import deque
import copy
import dataclasses
import logging
from typing import Union, Deque, TypeVar, Type

import jsonpickle
import sqlalchemy

from . import database, exceptions


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class ICommand:
    """ ICommand """


class ICommandHandler(abc.ABC):
    """ ICommandHandler """


@dataclasses.dataclass
class IQuery:
    """ IQuery """


class IQueryHandler(abc.ABC):
    """ IQueryHandler """


@dataclasses.dataclass
class IEvent:
    """ IEvent """


class IEventHandler(abc.ABC):
    """ IEventHandler """


class IAggregate(abc.ABC):
    """ IAggregate """

    def __init__(self) -> None:
        """ __init__ """
        self.aggregate_id: Union[None, str] = None
        self._queue: Deque[IEvent] = deque()
        self._history: Deque[IEvent] = deque()

    @abc.abstractmethod
    def apply(self, event: IEvent, skip=False) -> None:
        """ apply """
        raise NotImplementedError

    @abc.abstractmethod
    def load(self, event: IEvent) -> None:
        """ laod """
        raise NotImplementedError

    def get_events(self) -> Deque[IEvent]:
        """ get_events """
        return copy.deepcopy(self._queue)

    def publish(self) -> None:
        """ publish """
        while True:
            try:
                event = self._queue.popleft()
                self._history.append(event)
            except IndexError:
                break


T = TypeVar("T")


class IAggregateRepository(abc.ABC):
    """ IAggregateRepository """

    def __init__(self, db_context: database.DbContext,) -> None:
        """ __init__ """
        self.db_context = db_context

    @property
    @abc.abstractmethod
    def aggregate_type(self) -> Type[IAggregate]:
        """ aggregate_type """
        raise NotImplementedError

    async def get_by_id(self, aggregate_id: str, aggregate_type: Type[T]) -> T:
        """ get_by_id """
        query = (
            self.db_context.aggregates.select()
            .where(self.db_context.aggregates.c.aggregate_id == aggregate_id)
            .order_by(sqlalchemy.asc(self.db_context.aggregates.c.event_id))
        )
        found = await self.db_context.connection.connection.fetch_all(query=query)
        if found:
            agg = aggregate_type()
            for e in found:
                event = jsonpickle.decode(e.get("event"))
                agg.load(event)  # type: ignore
            return agg
        raise exceptions.AggregateNotFoundException()

    @abc.abstractmethod
    async def apply(self, event: IEvent) -> None:
        """ apply """
        raise NotImplementedError

    async def save(self, aggregate: IAggregate) -> None:
        """ save """
        events = aggregate.get_events()
        if not events:
            return
        while True:
            try:
                item = events.popleft()
            except IndexError:
                break
            query = self.db_context.aggregates.insert()
            values = {
                "aggregate_id": aggregate.aggregate_id,
                "event": jsonpickle.encode(item),
            }
            await self.db_context.connection.connection.execute(
                query=query, values=values
            )
            await self.apply(item)
        aggregate.publish()
