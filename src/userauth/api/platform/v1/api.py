""" api.py """


from fastapi import APIRouter


from .user import userapicontroller
from .song import songapicontroller


V1_ROUTER = APIRouter()
V1_ROUTER.include_router(userapicontroller.ROUTER, prefix="/user")
V1_ROUTER.include_router(songapicontroller.ROUTER, prefix="/song")
