""" likesongeventhandler.py """


import dataclasses
import datetime
import logging

import asyncpg

from ..... import aggregate, database
from . import user

_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class LikeSongEvent(aggregate.IEvent):
    """ LikeSongEvent """

    user_aggregate_id: str
    song_aggregate_id: str
    attitude_type: user.AttitudeType
    created_on: datetime.datetime


class LikeSongEventHandler(aggregate.IEventHandler):
    """ LikeSongEventHandler """

    def __init__(self, db_context: database.DbContext) -> None:
        """ __init__ """
        self.db_context = db_context

    async def handle(self, event: LikeSongEvent) -> None:
        """ handle """
        _logger.info("LikeSongEventHandler handler executed.")
        try:
            query = self.db_context.song_likes.insert()
            values = {
                "user_aggregate_id": event.user_aggregate_id,
                "song_aggregate_id": event.song_aggregate_id,
                "attitude_type": event.attitude_type,
                "created_on": event.created_on,
            }
            await self.db_context.connection.connection.execute(query=query, values=values)
        except asyncpg.exceptions.UniqueViolationError:
            query = self.db_context.song_likes.update().where(
                self.db_context.song_likes.c.user_aggregate_id == event.user_aggregate_id
            ).where(self.db_context.song_likes.c.song_aggregate_id == event.song_aggregate_id)
            values = {
                "attitude_type": event.attitude_type,
                "created_on": event.created_on,
            }
            await self.db_context.connection.connection.execute(query=query, values=values)
