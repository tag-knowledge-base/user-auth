""" songcreatedeventhandler.py """


import logging

from ..... import aggregate


_logger = logging.getLogger(__name__)


class SaveSongToUserEventHandler(aggregate.IEventHandler):
    """ SaveSongToUserEventHandler """

    def __init__(self) -> None:
        """ __init__ """

    async def handle(self, event: aggregate.IEvent) -> None:
        """ handle """
        _logger.info("SaveSongToUserEventHandler handler executed.")
