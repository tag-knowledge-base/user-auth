""" user.py """


import dataclasses
import datetime
from enum import Enum
from typing import List


class AttitudeType(str, Enum):
    """ AttitudeType """

    indifferent = "Indifferent"
    like = "Like"
    dislike = "Dislike"


class ContributionAction(str, Enum):
    """ ContributionAction """

    undefined = "Undefined"
    created = "Created"
    deleted = "Deleted"


class ContributionType(str, Enum):
    """ ContributionType """

    undefined = "Undefined"
    song = "Song"


@dataclasses.dataclass
class Contribution:
    """ Contribution """

    aggregate_id: str
    contributed_on: datetime.datetime
    contribution_type: ContributionType = ContributionType.undefined
    action_type: ContributionAction = ContributionAction.undefined


class MetaData:
    """ MetaData """

    def __init__(self, created_by: str, created_on: datetime.datetime) -> None:
        """ __init__ """
        self.created_by = created_by
        self.created_on = created_on
        self.last_modified_by = self.created_by
        self.last_modified_on = self.created_on

    def update(
        self, last_modified_by: str, last_modified_on: datetime.datetime
    ) -> None:
        """ update """
        self.last_modified_on = last_modified_on
        self.last_modified_by = last_modified_by


class SongAttitude:
    """ SongAttitude """

    def __init__(
        self,
        song_aggregate_id: str,
        attitude: AttitudeType,
        created_on: datetime.datetime,
    ) -> None:
        """ __init__ """
        self.song = song_aggregate_id
        self.attitude = attitude
        self.created_on = created_on


class User:
    """ User """

    def __init__(self, aggregate_id: str, created_on: datetime.datetime) -> None:
        """ __init__ """
        self.aggregate_id = aggregate_id
        self.metadata = MetaData(aggregate_id, created_on)
        self.contributions: List[Contribution] = []
        self.liked_songs: List[SongAttitude] = []

    def created_song(
        self, contribution: Contribution, created_on: datetime.datetime, created_by: str
    ) -> None:
        """ created_song """
        if self.contributions is None:
            self.contributions = []
        self.contributions.append(contribution)
        self.metadata.update(created_by, created_on)

    def create_song_attitude(
        self,
        song_aggregate_id: str,
        attitude: AttitudeType,
        created_on: datetime.datetime,
    ) -> None:
        """ create_song_attitude """
        song_dict = {i.song: index for index, i in enumerate(self.liked_songs)}
        if song_aggregate_id in song_dict:
            self.liked_songs[song_dict[song_aggregate_id]] = SongAttitude(
                song_aggregate_id, attitude, created_on
            )
        else:
            self.liked_songs.append(
                SongAttitude(song_aggregate_id, attitude, created_on)
            )
        self.metadata.update(self.aggregate_id, created_on)
