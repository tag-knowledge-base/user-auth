""" dtos.py """


import pydantic

from . import user


class LikeSongDto(pydantic.BaseModel):
    """ LikeSongDto """

    song_aggregate_id: str
    attitude_type: user.AttitudeType
