""" userdetailsaggregate.py """


import hashlib
import logging
from typing import Type, Union

from ..... import aggregate, database, exceptions
from . import (
    saveuseroncreateusereventhandler,
    songcreatedeventhandler,
    songdeletedeventhandler,
    user,
    likesongeventhandler,
)
from ..song import (
    createsongeventhandler,
    deletesongeventhandler,
)


_logger = logging.getLogger(__name__)


class UserAggregate(aggregate.IAggregate):
    """ UserAggregate """

    def __init__(self) -> None:
        """ __init__ """
        super().__init__()
        self.aggregate_id: Union[str, None] = None
        self.user: Union[user.User, None] = None

    @classmethod
    def generate_aggregate_id(cls, user_id: str) -> str:
        """ generate_aggregate_id """
        return hashlib.sha256(user_id.encode("UTF-8")).hexdigest()

    def apply_create_user(
        self, event: saveuseroncreateusereventhandler.SaveUserOnCreateUserEvent,
    ) -> None:
        """ apply_create_user """
        if self.user:
            raise exceptions.UserAlreadyExistsException()
        self.user = user.User(event.aggregate_id, event.created_on)
        self.aggregate_id = event.aggregate_id

    def apply_save_song_to_user(
        self, event: createsongeventhandler.CreateSongEvent
    ) -> None:
        """ apply_save_song_to_user """
        if not self.user or not self.aggregate_id:
            raise exceptions.UserDoesNotExistException()
        contribution = user.Contribution(
            event.aggregate_id,
            event.created_on,
            user.ContributionType.song,
            user.ContributionAction.created,
        )
        self.user.created_song(contribution, event.created_on, event.created_by)

    def apply_delete_song_by_user(
        self, event: deletesongeventhandler.DeleteSongEvent
    ) -> None:
        """ apply_delete_song_by_user """
        if not self.user or not self.aggregate_id:
            raise exceptions.UserDoesNotExistException()
        contribution = user.Contribution(
            event.song_aggregate_id,
            event.deleted_on,
            user.ContributionType.song,
            user.ContributionAction.deleted,
        )
        self.user.created_song(contribution, event.deleted_on, event.deleted_by)

    def apply_like_song(self, event: likesongeventhandler.LikeSongEvent) -> None:
        """ apply_like_song """
        if not self.user or not self.aggregate_id:
            raise exceptions.UserDoesNotExistException()
        self.user.create_song_attitude(
            event.song_aggregate_id, event.attitude_type, event.created_on
        )

    def apply(self, event: aggregate.IEvent, skip=False) -> None:
        """ apply """
        if isinstance(
            event, saveuseroncreateusereventhandler.SaveUserOnCreateUserEvent,
        ):
            self.apply_create_user(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent):
            self.apply_delete_song_by_user(event)
        elif isinstance(event, createsongeventhandler.CreateSongEvent,):
            self.apply_save_song_to_user(event)
        elif isinstance(event, likesongeventhandler.LikeSongEvent,):
            self.apply_like_song(event)
        else:
            _logger.exception("No apply event found for %s.", event)
            raise ValueError("No apply event found for %s.", event)
        if not skip:
            self._queue.append(event)

    def load(self, event: aggregate.IEvent) -> None:
        """ load """
        if isinstance(
            event, saveuseroncreateusereventhandler.SaveUserOnCreateUserEvent,
        ):
            self.apply_create_user(event)
        elif isinstance(event, createsongeventhandler.CreateSongEvent,):
            self.apply_save_song_to_user(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent):
            self.apply_delete_song_by_user(event)
        elif isinstance(event, likesongeventhandler.LikeSongEvent,):
            self.apply_like_song(event)
        else:
            _logger.exception("No apply event found for %s.", event)
            raise ValueError("No apply event found for %s.", event)
        self._history.append(event)


class UserAggregateRepository(aggregate.IAggregateRepository):
    """ UserAggregateRepository """

    def __init__(
        self,
        db_context: database.DbContext,
        save_user_on_create_user_event_handler: saveuseroncreateusereventhandler.SaveUserOnCreateUserEventHandler,
        delete_song_by_user_event_handler: songdeletedeventhandler.DeleteSongByUserEventHandler,
        save_song_to_user_event_handler: songcreatedeventhandler.SaveSongToUserEventHandler,
        like_song_event_handler: likesongeventhandler.LikeSongEventHandler,
    ) -> None:
        """ __init__.py """
        self.save_user_on_create_user_event_handler = (
            save_user_on_create_user_event_handler
        )
        self.delete_song_by_user_event_handler = delete_song_by_user_event_handler
        self.save_song_to_user_event_handler = save_song_to_user_event_handler
        self.like_song_event_handler = like_song_event_handler
        super().__init__(db_context,)

    @property
    def aggregate_type(self) -> Type[UserAggregate]:
        """ aggregate_type """
        return UserAggregate

    async def apply(self, event: aggregate.IEvent) -> None:
        """ apply """
        if isinstance(
            event, saveuseroncreateusereventhandler.SaveUserOnCreateUserEvent,
        ):
            await self.save_user_on_create_user_event_handler.handle(event)
        elif isinstance(event, createsongeventhandler.CreateSongEvent,):
            await self.save_song_to_user_event_handler.handle(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent,):
            await self.delete_song_by_user_event_handler.handle(event)
        elif isinstance(event, likesongeventhandler.LikeSongEvent,):
            await self.like_song_event_handler.handle(event)
        else:
            _logger.warning("No event handler found for %s.", event)
