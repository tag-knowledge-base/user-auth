""" purgreusercommandhandler.py """


import dataclasses
import logging

from ..... import aggregate, database
from . import userdetailsaggregate


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class ClearUserCommand(aggregate.ICommand):
    """ ClearUserCommand """

    user_id: str


class ClearUserCommandHandler(aggregate.ICommandHandler):
    """ ClearUserCommandHandler """

    def __init__(
        self,
        user_aggregate: userdetailsaggregate.UserAggregate,
        db_context: database.DbContext,
    ) -> None:
        """ __init__ """
        self.user_aggregate = user_aggregate
        self.db_context = db_context

    async def handle(self, command: ClearUserCommand) -> None:
        """ handle """
        aggregate_id = self.user_aggregate.generate_aggregate_id(command.user_id)
        query = self.db_context.aggregates.delete().where(
            self.db_context.aggregates.c.aggregate_id == aggregate_id
        )
        await self.db_context.connection.connection.execute(query=query)
