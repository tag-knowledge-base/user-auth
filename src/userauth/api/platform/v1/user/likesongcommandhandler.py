""" likesongcommandhandler.py """


import dataclasses
import datetime
from typing import List

from ..... import aggregate, exceptions
from . import userdetailsaggregate, likesongeventhandler, user
from ..user import userdetailsaggregate


@dataclasses.dataclass
class LikeSongCommand(aggregate.ICommand):
    """ LikeSongCommand """

    user_id: str
    song_aggregate_id: str
    attitude_type: user.AttitudeType
    created_on: datetime.datetime


class LikeSongCommandHandler(aggregate.ICommandHandler):
    """ LikeSongCommandHandler """

    def __init__(
        self,
        user_aggregate: userdetailsaggregate.UserAggregate,
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository,
    ) -> None:
        """ __init__ """
        self.user_aggregate = user_aggregate
        self.user_aggregate_repository = user_aggregate_repository

    async def handle(self, command: LikeSongCommand) -> None:
        """ handle """
        user_aggregate_id = self.user_aggregate.generate_aggregate_id(command.user_id)
        user_aggregate = await self.user_aggregate_repository.get_by_id(
            user_aggregate_id, self.user_aggregate_repository.aggregate_type
        )
        like_song_event = likesongeventhandler.LikeSongEvent(
            user_aggregate_id=user_aggregate_id,
            song_aggregate_id=command.song_aggregate_id,
            attitude_type=command.attitude_type,
            created_on=command.created_on,
        )
        user_aggregate.apply(like_song_event)
        await self.user_aggregate_repository.save(user_aggregate)
