""" userdataqueryhandler.py """


import dataclasses
import datetime
import logging
from typing import Union, List

import pydantic

from ..... import aggregate
from . import userdetailsaggregate, user


_logger = logging.getLogger(__name__)


class ContributionDto(pydantic.BaseModel):
    """ ContributionDto """

    aggregate_id: str
    contributed_on: datetime.datetime
    contribution_type: user.ContributionType = user.ContributionType.undefined
    action_type: user.ContributionAction = user.ContributionAction.undefined


class MetaData(pydantic.BaseModel):
    """ MetaData """

    created_by: str
    created_on: datetime.datetime
    last_modified_by: str
    last_modified_on: datetime.datetime


class SongAttitude(pydantic.BaseModel):
    """ SongAttitude """

    song: str
    attitude: user.AttitudeType
    created_on: datetime.datetime


class GetUserResponseDto(pydantic.BaseModel):
    """ GetUserResponseDto """

    aggregate_id: str
    metadata: Union[MetaData, None]
    contributions: List[ContributionDto] = []
    liked_songs: List[SongAttitude] = []


@dataclasses.dataclass
class GetUserQuery(aggregate.IQuery):
    """ GetUserQuery """

    user_id: str


class GetUserQueryHandler(aggregate.IQueryHandler):
    """ Handler """

    def __init__(
        self,
        user_aggregate: userdetailsaggregate.UserAggregate,
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository,
    ) -> None:
        """ __init__ """
        self.user_aggregate = user_aggregate
        self.user_aggregate_repository = user_aggregate_repository

    async def handle(self, query: GetUserQuery) -> GetUserResponseDto:
        """ handle """
        aggregate_id = self.user_aggregate.generate_aggregate_id(query.user_id)
        user_aggregate = await self.user_aggregate_repository.get_by_id(
            aggregate_id, self.user_aggregate_repository.aggregate_type
        )
        if user_aggregate.user:
            return GetUserResponseDto(
                aggregate_id=user_aggregate.aggregate_id,
                metadata=MetaData(
                    created_by=user_aggregate.user.metadata.created_by,
                    created_on=user_aggregate.user.metadata.created_on,
                    last_modified_by=user_aggregate.user.metadata.last_modified_by,
                    last_modified_on=user_aggregate.user.metadata.last_modified_on,
                ),
                contributions=[
                    ContributionDto(
                        aggregate_id=i.aggregate_id,
                        contributed_on=i.contributed_on,
                        contribution_type=i.contribution_type,
                        action_type=i.action_type,
                    )
                    for i in user_aggregate.user.contributions
                ],
                liked_songs=[
                    SongAttitude(
                        song=i.song,
                        attitude=i.attitude,
                        created_on=i.created_on
                    ) for i in user_aggregate.user.liked_songs
                ]
            )
        return GetUserResponseDto(aggregate_id=user_aggregate.aggregate_id,)
