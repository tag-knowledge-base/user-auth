""" userapicontroller """


import datetime
import logging

from fastapi import APIRouter, Header
import pytz

from ..... import exceptions, database
from ..... import dependencies as app_dependencies
from . import (
    createusercommandhandler,
    userdataqueryhandler,
    clearusercommandhandler,
    dtos,
    likesongcommandhandler,
)


_logger = logging.getLogger(__name__)


ROUTER = APIRouter()


@ROUTER.post("", response_model=str, tags=["user"])
async def async_create_user(user_id: str = Header(None)) -> str:
    """ async_create_user """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        command = createusercommandhandler.CreateUserCommand(
            user_id=user_id, created_on=datetime.datetime.now(pytz.UTC),
        )
        command_handler: createusercommandhandler.CreateUserCommandHandler = app_dependencies.APP_GRAPH.provide(
            createusercommandhandler.CreateUserCommandHandler
        )
        await command_handler.handle(command)
        return user_id


@ROUTER.get(
    "", response_model=userdataqueryhandler.GetUserResponseDto, tags=["user"],
)
async def async_get_user(
    user_id: str = Header(None),
) -> userdataqueryhandler.GetUserResponseDto:
    """ async_get_user """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        query = userdataqueryhandler.GetUserQuery(user_id=user_id,)
        query_handler: userdataqueryhandler.GetUserQueryHandler = app_dependencies.APP_GRAPH.provide(
            userdataqueryhandler.GetUserQueryHandler
        )
        return await query_handler.handle(query)


@ROUTER.delete("", response_model=str, tags=["user"])
async def async_clear_user(user_id: str = Header(None)) -> str:
    """ async_clear_user """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        command = clearusercommandhandler.ClearUserCommand(user_id=user_id,)
        command_handler: clearusercommandhandler.ClearUserCommandHandler = app_dependencies.APP_GRAPH.provide(
            clearusercommandhandler.ClearUserCommandHandler
        )
        await command_handler.handle(command)
        return user_id


@ROUTER.post("/song/like", response_model=dtos.LikeSongDto, tags=["user"])
async def async_like_song(
    like_song_dto: dtos.LikeSongDto, user_id: str = Header(None)
) -> dtos.LikeSongDto:
    """ async_like_song """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        command = likesongcommandhandler.LikeSongCommand(
            user_id=user_id,
            song_aggregate_id=like_song_dto.song_aggregate_id,
            attitude_type=like_song_dto.attitude_type,
            created_on=datetime.datetime.now(pytz.UTC),
        )
        command_handler: likesongcommandhandler.LikeSongCommandHandler = app_dependencies.APP_GRAPH.provide(
            likesongcommandhandler.LikeSongCommandHandler
        )
        await command_handler.handle(command)
    return like_song_dto
