""" createusercommandhandler.py """


import dataclasses
import datetime

from ..... import aggregate, exceptions
from . import (
    userdetailsaggregate,
    saveuseroncreateusereventhandler,
)


@dataclasses.dataclass
class CreateUserCommand(aggregate.ICommand):
    """ command """

    user_id: str
    created_on: datetime.datetime


class CreateUserCommandHandler(aggregate.ICommandHandler):
    """ Handler """

    def __init__(
        self,
        user_aggregate: userdetailsaggregate.UserAggregate,
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository,
    ) -> None:
        """ __init__ """
        self.user_aggregate = user_aggregate
        self.user_aggregate_repository = user_aggregate_repository

    async def handle(self, command: CreateUserCommand) -> None:
        """ handle """
        aggregate_id = self.user_aggregate.generate_aggregate_id(command.user_id)
        try:
            user_aggregate = await self.user_aggregate_repository.get_by_id(
                aggregate_id, self.user_aggregate_repository.aggregate_type
            )
        except exceptions.AggregateNotFoundException:
            user_aggregate = self.user_aggregate
        create_user_event = saveuseroncreateusereventhandler.SaveUserOnCreateUserEvent(
            aggregate_id=aggregate_id, created_on=command.created_on,
        )
        user_aggregate.apply(create_user_event)
        await self.user_aggregate_repository.save(user_aggregate)
