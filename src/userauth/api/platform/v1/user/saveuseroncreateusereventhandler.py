""" saveuseroncreateusereventhandler.py """


import dataclasses
import datetime
import logging

from ..... import aggregate


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class SaveUserOnCreateUserEvent(aggregate.IEvent):
    """ SaveUserOnCreateUserEvent """

    aggregate_id: str
    created_on: datetime.datetime


class SaveUserOnCreateUserEventHandler(aggregate.IEventHandler):
    """ Handler """

    def __init__(self) -> None:
        """ __init__ """

    async def handle(self, event: aggregate.IEvent) -> None:
        """ handle """
        _logger.info("SaveUserOnCreateUserEvent handler executed.")
