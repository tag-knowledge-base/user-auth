""" deletesongeventhandler.py """


import dataclasses
import datetime
import logging

from ..... import aggregate, database


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class DeleteSongEvent(aggregate.IEvent):
    """ DeleteSongEvent """

    song_aggregate_id: str
    deleted_on: datetime.datetime
    deleted_by: str


class DeleteSongEventHandler(aggregate.IEventHandler):
    """ DeleteSongEventHandler """

    def __init__(self, db_context: database.DbContext) -> None:
        """ __init__ """
        self.db_context = db_context

    async def handle(self, event: DeleteSongEvent) -> None:
        """ handle """
        _logger.info("DeleteSongEvent handler executed.")
        query = self.db_context.songs.delete().where(
            self.db_context.songs.c.aggregate_id == event.song_aggregate_id
        )
        await self.db_context.connection.connection.execute(query=query)
