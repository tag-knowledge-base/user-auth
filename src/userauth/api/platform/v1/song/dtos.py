""" dtos.py """


from typing import List

import pydantic


class CreateSongDto(pydantic.BaseModel):
    """ CreateSongDto """

    song: str
    artist: List[str]
    album: str
    track_position: int = 0


class SongDetailDto(pydantic.BaseModel):
    """ SongDetailDto """

    song: str
    artist: List[str]
    album: str
    track_position: int
    aggregate_id: str
