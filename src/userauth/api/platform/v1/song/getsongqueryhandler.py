""" getsongqueryhandler.py """


import dataclasses
import logging
from typing import List

from ..... import aggregate, database
from . import dtos


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class GetSongQuery(aggregate.IQuery):
    """ GetSongQuery """

    song_aggregate_id: str


class GetSongQueryHandler(aggregate.IQueryHandler):
    """ GetSongQueryHandler """

    def __init__(self, db_context: database.DbContext,) -> None:
        """ __init__ """
        self.db_context = db_context

    async def handle(self, query: GetSongQuery) -> List[dtos.SongDetailDto]:
        """ handle """
        _query = self.db_context.songs.select().where(
            self.db_context.songs.c.aggregate_id == query.song_aggregate_id
        )
        found = await self.db_context.connection.connection.fetch_one(query=_query)
        return [
            dtos.SongDetailDto(
                song=found.get("song"),
                artist=found.get("artist"),
                album=found.get("album"),
                track_position=found.get("track_position"),
                aggregate_id=found.get("aggregate_id"),
            )
        ]
