""" createsongeventhandler.py """


import dataclasses
import datetime
import logging
from typing import List

from ..... import aggregate, database


_logger = logging.getLogger(__name__)


@dataclasses.dataclass
class CreateSongEvent(aggregate.IEvent):
    """ CreateSongEvent """

    aggregate_id: str
    song: str
    artist: List[str]
    album: str
    track_position: int
    created_on: datetime.datetime
    created_by: str


class CreateSongEventHandler(aggregate.IEventHandler):
    """ CreateSongEventHandler """

    def __init__(self, db_context: database.DbContext) -> None:
        """ __init__ """
        self.db_context = db_context

    async def handle(self, event: CreateSongEvent) -> None:
        """ handle """
        _logger.info("CreateSongEvent handler executed.")
        query = self.db_context.songs.insert()
        values = {
            "aggregate_id": event.aggregate_id,
            "song": event.song,
            "album": event.album,
            "artist": event.artist,
            "track_position": event.track_position,
        }
        await self.db_context.connection.connection.execute(query=query, values=values)
