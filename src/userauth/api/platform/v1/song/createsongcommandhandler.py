""" createsongcommandhandler.py """


import dataclasses
import datetime
from typing import List

from ..... import aggregate, exceptions
from . import (
    songdetailsaggregate,
    createsongeventhandler,
)
from ..user import userdetailsaggregate


@dataclasses.dataclass
class CreateSongCommand(aggregate.ICommand):
    """ CreateSongCommand """

    song: str
    artist: List[str]
    album: str
    track_position: int
    created_by: str
    created_on: datetime.datetime


class CreateSongCommandHandler(aggregate.ICommandHandler):
    """ CreateSongCommandHandler """

    def __init__(
        self,
        song_aggregate: songdetailsaggregate.SongAggregate,
        song_aggregate_repository: songdetailsaggregate.SongAggregateRepository,
        user_aggregate: userdetailsaggregate.UserAggregate,
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository,
    ) -> None:
        """ __init__ """
        self.song_aggregate = song_aggregate
        self.song_aggregate_repository = song_aggregate_repository
        self.user_aggregate = user_aggregate
        self.user_aggregate_repository = user_aggregate_repository

    async def handle(self, command: CreateSongCommand) -> None:
        """ handle """
        song_aggregate_id = self.song_aggregate.generate_aggregate_id(
            command.song, sorted(command.artist), command.album, command.track_position,
        )
        try:
            song_aggregate = await self.song_aggregate_repository.get_by_id(
                song_aggregate_id, self.song_aggregate_repository.aggregate_type
            )
        except exceptions.AggregateNotFoundException:
            song_aggregate = self.song_aggregate
        create_song_event = createsongeventhandler.CreateSongEvent(
            aggregate_id=song_aggregate_id,
            song=command.song,
            artist=sorted(command.artist),
            album=command.album,
            track_position=command.track_position,
            created_on=command.created_on,
            created_by=command.created_by,
        )
        song_aggregate.apply(create_song_event)
        user_aggregate = await self.user_aggregate_repository.get_by_id(
            command.created_by, userdetailsaggregate.UserAggregate,
        )
        user_aggregate.apply(create_song_event)
        await self.song_aggregate_repository.save(song_aggregate)
        await self.user_aggregate_repository.save(user_aggregate)
