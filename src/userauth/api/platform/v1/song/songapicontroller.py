""" songapicontroller """


import datetime
import logging
from typing import List

from fastapi import APIRouter, Header
import pytz

from . import (
    dtos,
    createsongcommandhandler,
    deletesongcommandhandler,
    getsongqueryhandler,
    getallsongsqueryhandler,
)
from ..user import userdetailsaggregate
from ..... import exceptions, database
from ..... import dependencies as app_dependencies


_logger = logging.getLogger(__name__)


ROUTER = APIRouter()


@ROUTER.post("", response_model=dtos.CreateSongDto, tags=["song"])
async def async_create_song(
    create_song_dto: dtos.CreateSongDto, user_id: str = Header(None)
) -> dtos.CreateSongDto:
    """ async_create_song """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        user_aggregate: userdetailsaggregate.UserAggregate = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregate
        )
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregateRepository
        )
        user = await user_aggregate_repository.get_by_id(
            user_aggregate.generate_aggregate_id(user_id),
            userdetailsaggregate.UserAggregate,
        )
        create_song_command = createsongcommandhandler.CreateSongCommand(
            song=create_song_dto.song,
            artist=create_song_dto.artist,
            album=create_song_dto.album,
            track_position=create_song_dto.track_position,
            created_on=datetime.datetime.now(pytz.UTC),
            created_by=user.aggregate_id,
        )
        song_command_handler: createsongcommandhandler.CreateSongCommandHandler = app_dependencies.APP_GRAPH.provide(
            createsongcommandhandler.CreateSongCommandHandler
        )
        await song_command_handler.handle(create_song_command)
        return create_song_dto


@ROUTER.get(
    "/{song_aggregate_id}", response_model=List[dtos.SongDetailDto], tags=["song"],
)
async def async_get_song(
    song_aggregate_id: str, user_id: str = Header(None)
) -> List[dtos.SongDetailDto]:
    """ async_get_user """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        user_aggregate: userdetailsaggregate.UserAggregate = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregate
        )
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregateRepository
        )
        user = await user_aggregate_repository.get_by_id(
            user_aggregate.generate_aggregate_id(user_id),
            userdetailsaggregate.UserAggregate,
        )
        song_query = getsongqueryhandler.GetSongQuery(
            song_aggregate_id=song_aggregate_id,
        )
        song_query_handler: getsongqueryhandler.GetSongQueryHandler = app_dependencies.APP_GRAPH.provide(
            getsongqueryhandler.GetSongQueryHandler
        )
        return await song_query_handler.handle(song_query)


@ROUTER.get(
    "", response_model=List[dtos.SongDetailDto], tags=["song"],
)
async def async_get_all_songs(user_id: str = Header(None)) -> List[dtos.SongDetailDto]:
    """ async_get_all_songs """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        user_aggregate: userdetailsaggregate.UserAggregate = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregate
        )
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregateRepository
        )
        user = await user_aggregate_repository.get_by_id(
            user_aggregate.generate_aggregate_id(user_id),
            userdetailsaggregate.UserAggregate,
        )
        song_query_handler: getallsongsqueryhandler.GetAllSongsQueryHandler = app_dependencies.APP_GRAPH.provide(
            getallsongsqueryhandler.GetAllSongsQueryHandler
        )
        return await song_query_handler.handle()


@ROUTER.delete("/{song_aggregate_id}", response_model=str, tags=["song"])
async def async_delete_song(song_aggregate_id: str, user_id: str = Header(None)) -> str:
    """ async_delete_song """
    if not user_id:
        raise exceptions.UserHeaderNotFoundException()
    db_context: database.DbContext = app_dependencies.APP_GRAPH.provide(
        database.DbContext
    )
    async with db_context.connection.connection.connection():
        user_aggregate: userdetailsaggregate.UserAggregate = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregate
        )
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository = app_dependencies.APP_GRAPH.provide(
            userdetailsaggregate.UserAggregateRepository
        )
        user = await user_aggregate_repository.get_by_id(
            user_aggregate.generate_aggregate_id(user_id),
            userdetailsaggregate.UserAggregate,
        )
        delete_song_command = deletesongcommandhandler.DeleteSongCommand(
            song_aggregate_id=song_aggregate_id,
            deleted_on=datetime.datetime.now(pytz.UTC),
            deleted_by=user.aggregate_id,
        )
        song_command_handler: deletesongcommandhandler.DeleteSongCommandHandler = app_dependencies.APP_GRAPH.provide(
            deletesongcommandhandler.DeleteSongCommandHandler
        )
        await song_command_handler.handle(delete_song_command)
        return song_aggregate_id
