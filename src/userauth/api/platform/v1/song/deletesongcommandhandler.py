""" deletesongcommandhandler.py """


import dataclasses
import datetime

from ..... import aggregate, exceptions
from . import (
    songdetailsaggregate,
    deletesongeventhandler,
)
from ..user import userdetailsaggregate


@dataclasses.dataclass
class DeleteSongCommand(aggregate.ICommand):
    """ DeleteSongCommand """

    song_aggregate_id: str
    deleted_by: str
    deleted_on: datetime.datetime


class DeleteSongCommandHandler(aggregate.ICommandHandler):
    """ DeleteSongCommandHandler """

    def __init__(
        self,
        song_aggregate: songdetailsaggregate.SongAggregate,
        song_aggregate_repository: songdetailsaggregate.SongAggregateRepository,
        user_aggregate: userdetailsaggregate.UserAggregate,
        user_aggregate_repository: userdetailsaggregate.UserAggregateRepository,
    ) -> None:
        """ __init__ """
        self.song_aggregate = song_aggregate
        self.song_aggregate_repository = song_aggregate_repository
        self.user_aggregate = user_aggregate
        self.user_aggregate_repository = user_aggregate_repository

    async def handle(self, command: DeleteSongCommand) -> None:
        """ handle """
        song_aggregate = await self.song_aggregate_repository.get_by_id(
            command.song_aggregate_id, self.song_aggregate_repository.aggregate_type
        )
        if song_aggregate.song is None:
            raise exceptions.SongDoesNotExistException()
        delete_song_event = deletesongeventhandler.DeleteSongEvent(
            song_aggregate_id=command.song_aggregate_id,
            deleted_by=command.deleted_by,
            deleted_on=command.deleted_on,
        )
        song_aggregate.apply(delete_song_event)
        user_aggregate = await self.user_aggregate_repository.get_by_id(
            command.deleted_by, userdetailsaggregate.UserAggregate,
        )
        user_aggregate.apply(delete_song_event)
        await self.song_aggregate_repository.save(song_aggregate)
        await self.user_aggregate_repository.save(user_aggregate)
