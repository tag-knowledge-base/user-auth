""" tagdetailsaggregate.py """


import hashlib
import json
import logging
from typing import Type, Union, List

from ..... import aggregate, database, exceptions
from . import (
    song,
    createsongeventhandler,
    deletesongeventhandler,
)


_logger = logging.getLogger(__name__)


class SongAggregate(aggregate.IAggregate):
    """ SongAggregate """

    def __init__(self) -> None:
        """ __init__ """
        super().__init__()
        self.aggregate_id: Union[str, None] = None
        self.song: Union[song.Song, None] = None

    @classmethod
    def generate_aggregate_id(
        cls, song: str, artist: List[str], album: str, track_position: int = 0
    ) -> str:
        """ generate_aggregate_id """
        stringy = json.dumps(
            {
                "song": song,
                "artist": sorted(artist),
                "album": album,
                "track_position": track_position,
            }
        )
        return hashlib.sha256(stringy.encode("UTF-8")).hexdigest()

    def apply_create_song(self, event: createsongeventhandler.CreateSongEvent) -> None:
        """ apply_create_song """
        if self.song:
            raise exceptions.SongAlreadyExistsExceptions()
        self.song = song.Song(
            event.aggregate_id,
            event.song,
            sorted(event.artist),
            event.album,
            event.created_by,
            event.created_on,
            event.track_position,
        )
        self.aggregate_id = event.aggregate_id

    def apply_delete_song(self, event: deletesongeventhandler.DeleteSongEvent) -> None:
        """ apply_delete_song """
        self.song = None

    def apply(self, event: aggregate.IEvent, skip=False) -> None:
        """ apply """
        if isinstance(event, createsongeventhandler.CreateSongEvent):
            self.apply_create_song(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent):
            self.apply_delete_song(event)
        else:
            _logger.exception("No apply event found for %s.", event)
            raise ValueError("No apply event found for %s.", event)
        if not skip:
            self._queue.append(event)

    def load(self, event: aggregate.IEvent) -> None:
        """ load """
        if isinstance(event, createsongeventhandler.CreateSongEvent):
            self.apply_create_song(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent):
            self.apply_delete_song(event)
        else:
            _logger.exception("No apply event found for %s.", event)
            raise ValueError("No appyly event found for %s.", event)
        self._history.append(event)


class SongAggregateRepository(aggregate.IAggregateRepository):
    """ SongAggregateRepository """

    def __init__(
        self,
        db_context: database.DbContext,
        delete_song_event_handler: deletesongeventhandler.DeleteSongEventHandler,
        create_song_event_handler: createsongeventhandler.CreateSongEventHandler,
    ) -> None:
        """ __init__.py """
        self.create_song_event_handler = create_song_event_handler
        self.delete_song_event_handler = delete_song_event_handler
        super().__init__(db_context,)

    @property
    def aggregate_type(self) -> Type[SongAggregate]:
        """ aggregate_type """
        return SongAggregate

    async def apply(self, event: aggregate.IEvent) -> None:
        """ apply """
        if isinstance(event, createsongeventhandler.CreateSongEvent):
            await self.create_song_event_handler.handle(event)
        elif isinstance(event, deletesongeventhandler.DeleteSongEvent):
            await self.delete_song_event_handler.handle(event)
        else:
            _logger.warning("No event handler found for %s.", event)
