""" getallsongsqueryhandler.py """


import logging
from typing import List

from ..... import aggregate, database
from . import dtos


_logger = logging.getLogger(__name__)


class GetAllSongsQueryHandler(aggregate.IQueryHandler):
    """ GetAllSongsQueryHandler """

    def __init__(self, db_context: database.DbContext,) -> None:
        """ __init__ """
        self.db_context = db_context

    async def handle(self) -> List[dtos.SongDetailDto]:
        """ handle """
        _query = self.db_context.songs.select()
        all_found = await self.db_context.connection.connection.fetch_all(query=_query)
        return [
            dtos.SongDetailDto(
                song=found.get("song"),
                artist=found.get("artist"),
                album=found.get("album"),
                track_position=found.get("track_position"),
                aggregate_id=found.get("aggregate_id"),
            )
            for found in all_found
        ]
