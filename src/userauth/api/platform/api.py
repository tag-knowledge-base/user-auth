""" api.py """


from fastapi import APIRouter


from .v1 import api

PLATFORM_ROUTER = APIRouter()
PLATFORM_ROUTER.include_router(api.V1_ROUTER, prefix="/v1")
