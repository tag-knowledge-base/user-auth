""" api.py """


from fastapi import APIRouter


from .platform import api

API_ROUTER = APIRouter()
API_ROUTER.include_router(api.PLATFORM_ROUTER, prefix="/platform")
