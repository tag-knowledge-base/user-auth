""" settings.py """


import os
from pydantic import BaseModel  # pylint: disable=E0401


class Configuration(BaseModel):  # pylint: disable=too-few-public-methods
    """ configuration """

    APP_NAME = os.getenv("APP_NAME", "userauthservice")
    APP_VERSION = os.getenv("APP_VERSION", "1.0.0-rc.1")
    AUTH_DB_USER = os.getenv("AUTH_DB_USER", "be_tag_user")
    AUTH_DB_PASSWORD = os.getenv("AUTH_DB_PASSWORD", "be_tag_user")
    AUTH_DB_NAME = os.getenv("AUTH_DB_NAME", "authuserdb")
    AUTH_DB_HOST = os.getenv("AUTH_DB_HOST", "localhost")
    AUTH_DB_PORT = os.getenv("AUTH_DB_PORT", "5432")
    AUTH_ALEMBIC_SCRIPT_LOCATION = os.getenv("AUTH_ALEMBIC_SCRIPT_LOCATION", "userauth")


CONFIGURATION = Configuration()
