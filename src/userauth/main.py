""" main.py """


import json
import logging
from pathlib import Path

import alembic.command
import alembic.config
from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

import logenv.utils

from . import config, exceptions, database, dependencies
from .api import api


APP_CONFIG = config.APP_CONFIG
logenv.utils.setup(logenv.utils.Configuration(**APP_CONFIG.get_value("logging")))
_logger = logging.getLogger(__name__)  # pylint: disable=invalid-name
_logger.info("Configuration %s", json.dumps(APP_CONFIG._dict))


APP = FastAPI()


@APP.on_event("startup")
async def startup() -> None:
    """ startup """
    alembic_cfg = alembic.config.Config(str(Path(__file__).parent / "alembic.ini"))
    alembic_cfg.set_main_option(
        "script_location", APP_CONFIG.get_value("AUTH_ALEMBIC_SCRIPT_LOCATION")
    )
    alembic.command.upgrade(alembic_cfg, "head")
    db_context: database.DbContext = dependencies.APP_GRAPH.provide(database.DbContext)
    await db_context.connection.connection.connect()
    _logger.info("Successfully started server")


@APP.on_event("shutdown")
async def shutdown() -> None:
    """ shutdown """
    db_context: database.DbContext = dependencies.APP_GRAPH.provide(database.DbContext)
    await db_context.connection.connection.disconnect()
    _logger.info("Successfully shutdown server")


@APP.exception_handler(exceptions.AiServiceException)
async def async_ai_service_exception_handler(
    request: Request, exc: exceptions.AiServiceException
):
    """ async_ai_service_exception_handler """
    assert request
    return JSONResponse(
        status_code=exc.status_code,
        content={"localization_key": exc.localization_key, "message": exc.message,},
    )


@APP.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    """ http_exception_handler """
    assert request
    assert exc
    _logger.exception("Validation exception")
    err = exceptions.InternalServerError()
    return JSONResponse(
        {"localization_key": err.localization_key, "message": err.message,},
        status_code=err.status_code,
    )


@APP.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    """ validation_exception_handler """
    assert request
    assert exc
    _logger.exception("Validation exception")
    err = exceptions.InternalServerError()
    return JSONResponse(
        {"localization_key": err.localization_key, "message": err.message,},
        status_code=err.status_code,
    )


APP.include_router(api.API_ROUTER, prefix="/api")
